package com.example.mybatis3.employee.jndimybatisemployee;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MappedTypes(Employee.class)
@MapperScan("com.example.mybatis3.employee.jndimybatisemployee.mapper")
public class JndiMybatisEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JndiMybatisEmployeeApplication.class, args);
	}
}
