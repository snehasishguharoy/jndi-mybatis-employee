/**
 * 
 */
package com.example.mybatis3.employee.jndimybatisemployee.intializer;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.example.mybatis3.employee.jndimybatisemployee.JndiMybatisEmployeeApplication;

/**
 * @author sony
 *
 */
public class EmployeeApplicationIntializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(JndiMybatisEmployeeApplication.class);
	}

}
