/**
 * 
 */
package com.example.mybatis3.employee.jndimybatisemployee.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

/**
 * @author sony
 *
 */

@Configuration
@EnableConfigurationProperties
public class AppConfig {
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource.primary")
	public JndiPropertyHolder primary() {
		return new JndiPropertyHolder();
	}

	@Bean
	@Primary
	public DataSource primaryDataSource() {
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		DataSource dataSource = (DataSource) dataSourceLookup.getDataSource(primary().getJndiName());
		return dataSource;
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(primaryDataSource());
		return factoryBean.getObject();
	}

}
