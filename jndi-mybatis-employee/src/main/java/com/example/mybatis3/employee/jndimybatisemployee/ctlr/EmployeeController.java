/**
 * 
 */
package com.example.mybatis3.employee.jndimybatisemployee.ctlr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mybatis3.employee.jndimybatisemployee.Employee;
import com.example.mybatis3.employee.jndimybatisemployee.mapper.EmployeeMapper;

/**
 * @author sony
 *
 */
@RestController
@RequestMapping("/rest/employees")

public class EmployeeController {

	/** The usermapper. */
	@Autowired
	private EmployeeMapper employeeMapper;

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@GetMapping("/all")
	public List<Employee> getAll() {
		return employeeMapper.getAll();
	}

	/**
	 * Gets the by id.
	 *
	 * @param id
	 *            the id
	 * @return the by id
	 */
	@GetMapping("/getemployeebyid/{id}")
	public Employee getById(@PathVariable("id") Integer id) {
		return employeeMapper.getById(id);
	}

	/**
	 * Insert employee.
	 *
	 * @param employee
	 *            the employee
	 */
	@PostMapping("/insertemployee")
	public void insertEmployee(@RequestBody Employee employee) {
		employeeMapper.insertEmployee(employee);
	}

	/**
	 * Delete employee.
	 *
	 * @param empId
	 *            the emp id
	 */
	@DeleteMapping("/deleteemployee/{empId}")
	public void deleteEmployee(@PathVariable("empId") Integer empId) {
		employeeMapper.deleteEmployee(empId);
	}

	/**
	 * Update employee.
	 *
	 * @param empId
	 *            the emp id
	 * @param employee
	 *            the employee
	 */
	@PostMapping("/updateemployee/{empId}")
	public void updateEmployee(@PathVariable("empId") Integer empId, @RequestBody Employee employee) {
		employeeMapper.updateEmployee(employee);
	}

}
