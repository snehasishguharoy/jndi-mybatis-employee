/**
 * 
 */
package com.example.mybatis3.employee.jndimybatisemployee.config;

/**
 * @author sony
 *
 */
public class JndiPropertyHolder {
	
	private String jndiName;

    public String getJndiName() {
        return jndiName;
    }

    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

}
